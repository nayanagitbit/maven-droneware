package tests;

import java.awt.AWTException;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

import utility.TestUtil;

//import utility.TestUtil;
import com.mongodb.DB;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;


public class DronewareUserTest {
	
	WebDriver driver;
	DB db;
	MongoClient mongoClient = null;
			  
	@BeforeTest
	//Driver Initialization, Login & Database Connectivity
	@Parameters({"url","useremail","userpwd", "drivertype", "driverpath", "host", "user", "password", "port", "dbname", "dbuser", "dbpwd", "csvfilepath", "ftpfilepath"})
	public void TestSetup(String url, String useremail, String userpwd, String drivertype, String driverpath, String host, String user, String password, int port, String dbname, String dbuser, String dbpwd, String csvfilepath, String ftpfilepath) throws InterruptedException {
		
		//JSch allows to connect to SSH(Secure Socket Shell) server
	    JSch jsch = new JSch();
	    //Session class stores session information for each user for security
	    Session session;		
		Boolean LoginCheck;
				
		System.setProperty(drivertype, driverpath);
		driver = new ChromeDriver();
		driver.get(url);
	    driver.manage().window().maximize();
	    driver.manage().deleteAllCookies();
	    driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.findElement(By.id("email")).sendKeys(useremail);
		driver.findElement(By.id("pwd")).sendKeys(userpwd);
		driver.findElement(By.id("login_btn")).click();
		
		LoginCheck = driver.findElement(By.xpath("//div[@class='user_name']")).isDisplayed();

		if(LoginCheck){
			  System.out.println("Login Successful");
		  }
	    else
		  {
			  System.out.println("Login is not successful");
		  }
		
		  
		 //Database Connectivity
		 try {
			 	//Proving the SSH details. GetSession returns the current HttpSession
			    session = jsch.getSession(user, host, port);
			 	session.setPassword(password);
			 	//Disabling the HostKey Checking
			 	java.util.Properties config = new java.util.Properties(); 
			 	config.put("StrictHostKeyChecking", "no");
			 	session.setConfig(config);
			 	session.connect();
			 	//setPortForwardingL() registers the local port forwarding. Here bind address is 'localhost' so listening port will be bound for local use only
			 	int forwardedPort = session.setPortForwardingL(0, "localhost", 27017);

			 	//To solve logger issue
			 	Logger mongoLogger = Logger.getLogger( "org.mongodb.driver" );
			 	mongoLogger.setLevel(Level.SEVERE);
		    
			 	//Creating the Mongo Credential instance by providing database details.
			 	MongoCredential mongoCredential = MongoCredential.createCredential(dbuser, dbname, dbpwd.toCharArray());

			 	//Passing the server address list to the mongo client constructor in order to connect 
			 	mongoClient = new MongoClient(new ServerAddress("localhost", forwardedPort), Arrays.asList(mongoCredential));
		
			 	//Reading the database
			 	db = mongoClient.getDB("droneware");
	 			
		 	} catch (JSchException e) {
		 		
		 		e.printStackTrace();
	 	}
		 
		//Connecting to FTP and upload file.
		JSch ftpJsch = new JSch();
		Session ftpSession = null;
		  
		try {
			  ftpSession = ftpJsch.getSession(user, host, port);
			  ftpSession.setConfig("StrictHostKeyChecking", "no");
			  ftpSession.setPassword(password);
			  ftpSession.connect();
		      Channel channel = ftpSession.openChannel("sftp");
		      channel.connect();
		      ChannelSftp sftpChannel = (ChannelSftp) channel;
		      String date, newdate;
		      Date d = new Date(System.currentTimeMillis());
			  date = d.toString();
			  newdate =  date.replaceAll("\\s+", "");
			  System.out.println(newdate);
		      
		      File tmpFileObj = new File(csvfilepath+".csv");
		      File zipFileObj = new File(csvfilepath+"-"+newdate+".csv");
		      zipFileObj.renameTo(tmpFileObj);

		      boolean varCanread = tmpFileObj.canRead();
		      boolean varFileName = tmpFileObj.renameTo(zipFileObj);
		      System.out.println(varCanread);
		      System.out.println(varFileName); 
		      
		      sftpChannel.put(csvfilepath+"-"+newdate+".csv", ftpfilepath);  
		      sftpChannel.exit();
		      ftpSession.disconnect();
		      System.out.println("File uploaded");
		      
		      boolean varFileName2 = zipFileObj.renameTo(tmpFileObj);
		      System.out.println(varFileName2);
		      
		  } catch (JSchException e) {
		      e.printStackTrace();  
		  } catch (SftpException e) {
		      e.printStackTrace();
		    Thread.sleep(2000);
		  }
	 	  
   }
	
	//Add Drone
	@DataProvider 
	public Iterator<Object[]> getDRONETestData() {
		ArrayList<Object[]> testDRONEData = TestUtil.getDRONEDataFromExcel();
		return testDRONEData.iterator();	  
	  }
  
	@Test(dataProvider = "getDRONETestData", priority =1)	
	public void AddDrone(String droneLogoPara, String connectionTypePara, String droneIpPara, String batteryChargePara, String warehousePara, String modalNumberPara, String serialNumberPara, String macAddress, String otherDetailsPara) throws InterruptedException, AWTException, IOException {
  
		Boolean droneAddCheck;
		String dronedate, dronenewdate;
		driver.findElement(By.id("3")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("add_drone_btn")).click();
		Thread.sleep(2000);
  
		//Uploading the logo
		driver.findElement(By.id("add_select_drone_picture")).click();
		Thread.sleep(3000);	
		StringSelection strPath = new StringSelection(droneLogoPara);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strPath, null);
  
		//Creating an object for robot class
		Robot robot = new Robot();
		//Ctrl+V ICS Config
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);			
		Thread.sleep(3000);
  
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER); 
  
		//Selecting the connection type
		WebElement droneconntype = driver.findElement(By.id("add_select_drone_connection_type"));
		Select dronetype = new Select(droneconntype);
		Thread.sleep(2000);
		dronetype.selectByVisibleText(connectionTypePara);
		Thread.sleep(2000);
  
		//Entering drone ip 
		driver.findElement(By.name("add_drone_ip")).sendKeys(droneIpPara);
		//Entering the battery charge %
		driver.findElement(By.id("select_drone_battery")).sendKeys(batteryChargePara);
		//Selecting the warehouse
		WebElement selwarehouse = driver.findElement(By.id("add_select_warehouse"));
		Select warehouse = new Select(selwarehouse);
		warehouse.selectByVisibleText(warehousePara);
		Thread.sleep(2000);
  
		//Entering the modal number
		driver.findElement(By.id("add_modal_number")).sendKeys(modalNumberPara);
		Thread.sleep(2000);
		//Entering the serial number
		Date d = new Date(System.currentTimeMillis());
		dronedate = d.toString();
		dronenewdate =  dronedate.replaceAll("\\s+","");
		System.out.println(dronenewdate);
		driver.findElement(By.id("add_serial_number")).sendKeys(serialNumberPara+dronenewdate);
		Thread.sleep(2000);
		//Entering the mac address
		driver.findElement(By.id("add_mac_address")).sendKeys(macAddress);
		Thread.sleep(2000);
		//Entering the other details
		driver.findElement(By.id("add_other_detail")).sendKeys(otherDetailsPara);
		Thread.sleep(2000);
		//Clicking the 'Save' button
		driver.findElement(By.id("add_drone_submit_btn")).click();
		Thread.sleep(5000);
		//Clicking the 'View Drone Details' button
		driver.findElement(By.id("drone_details_view_btn0")).click();
		Thread.sleep(2000);
  	  
		//Checking drone is added successfully or not.
		droneAddCheck = driver.findElement(By.xpath("//td[contains(text(),'CE1010')]")).isDisplayed();

		if(droneAddCheck){
			System.out.println("Drone is added successfully");
			}
		else
			{
		  System.out.println("Drone isn't added successfully");
			}
  
		//Taking the screenshot of drone screen
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/screenshots/DroneScreen.png"),true);
  
		driver.findElement(By.id("close_drone_details_modal_btn")).click();
  	  
		//To verify the Drone in database
		//Storing the collection information in the collection object
		DBCollection collection = db.getCollection("drones");	 
		//BasicDBObject can be used to represent a document in mongodb
		BasicDBObject searchQuery = new BasicDBObject();
		BasicDBObject field1 = new BasicDBObject();
		BasicDBObject field2 = new BasicDBObject();
		//Query to find by serial number
		searchQuery.put("serial_number", serialNumberPara+dronenewdate);	  	  
  
		//Query to read the information of serial number field
		field1.put("serial_number","1");
		
		//Query to read the information of drone status field	  
		field2.put("drone_status","1");
  
		//Cursor is pointer to the collection of documents. Cursor will be iterated automatically when the result of the query is returned.
		DBCursor cursor1 = collection.find(searchQuery, field1);
		DBCursor cursor2 = collection.find(searchQuery, field2);
  
		while (cursor1.hasNext()) {
			System.out.println(cursor1.next());
			
		}
	    while (cursor2.hasNext()) {
	    	System.out.println(cursor2.next());
		    //collection.remove(searchQuery);
  	}
 }
	//Add (User Side) User
	@DataProvider 
	public Iterator<Object[]> getDroneUSERTestData() {
		ArrayList<Object[]> testDroneUSERData = TestUtil.getDroneUSERDataFromExcel();
		return testDroneUSERData.iterator();	  
	}
	@Test (dataProvider = "getDroneUSERTestData", priority= 2)
	public void AddDroneUser(String dfirstNamePara, String dlastNamePara, String demailPara, String duserTypePara) throws InterruptedException, IOException {
		String userdate, usernewdate;
		Boolean userAddCheck;
		//Clicking on 'Users' button
		driver.findElement(By.id("4")).click();    	
    	Thread.sleep(2000);
    	//Clicking on 'Add User' button
    	driver.findElement(By.id("add_user_btn")).click();    	
    	Thread.sleep(2000);
    	//Entering First Name
    	driver.findElement(By.id("add_user_first_name")).sendKeys(dfirstNamePara);    	
    	Thread.sleep(2000);
    	//Entering Last Name
    	driver.findElement(By.id("add_user_last_name")).sendKeys(dlastNamePara);    	
    	Thread.sleep(2000);
    	//Creating the time stamp
    	Date d = new Date(System.currentTimeMillis());
    	userdate = d.toString();
    	usernewdate =  userdate.replaceAll("\\s+", "");
    	System.out.println(usernewdate);
    	//Entering Email Id
    	driver.findElement(By.id("add_user_email")).sendKeys(demailPara+usernewdate);    	
    	Thread.sleep(2000);
    	//Selecting the User Type
    	WebElement el = driver.findElement(By.id("add_select_user_type"));
    	Select sl= new Select(el);
    	sl.selectByVisibleText(duserTypePara);
    	//Clicking the 'Save' button
    	driver.findElement(By.id("add_user_submit_btn")).click();
    	driver.navigate().refresh();
    	
    	//Checking user is added successfully or not.
    	userAddCheck = driver.findElement(By.xpath("//td[contains(text(),'AutoRutuja')]")).isDisplayed();

    	if(userAddCheck){
    			System.out.println("User is added successfully");
    		}
    	else
    		{
    			System.out.println("User isn't added successfully");
    		}
    	  
    	//Taking the screenshot of drone screen
    	File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    	FileUtils.copyFile(scrFile, new File("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/screenshots/DroneUserScreen.png"),true);
    	  
    	
    	//To verify the User in database
    	//Storing the collection information in the collection object
    	DBCollection collection = db.getCollection("users");
		BasicDBObject searchQuery = new BasicDBObject();
		BasicDBObject field1 = new BasicDBObject();
		BasicDBObject field2 = new BasicDBObject();
		
		searchQuery.put("email", "rutuja4392@gmail.com"+usernewdate);
		field1.put("first_name","1");	
		field2.put("last_name","1");
		
		DBCursor cursor1 = collection.find(searchQuery, field1);
		DBCursor cursor2 = collection.find(searchQuery, field2);
			 
		while (cursor1.hasNext()) {
			System.out.println(cursor1.next());
		}
		
		while (cursor2.hasNext()) {
			System.out.println(cursor2.next());
			//collection.remove(searchQuery);
		}
		
	}
	
	//Add User Created Mission
	@DataProvider 
	public Iterator<Object[]> getUserMISSIONTestData() {
		ArrayList<Object[]> testUserMISSIONData = TestUtil.getUserMISSIONDataFromExcel();
		return testUserMISSIONData.iterator();	  
	}
	@Test(dataProvider = "getUserMISSIONTestData", priority= 3)
	public void AddUserCreatedMission(String missionNamePara, String missionModePara, String warehousePara, String runtimePositionPara, String invDocument0Para, String storageBinPara, String productName0Para, String serialNumber0Para, String invDocument1Para, String aisle1Para, String bay1Para, String level1Para, String position1Para, String productName1Para, String serialNumber1Para) throws InterruptedException, IOException {
		String userMissionDate, userMissionNewdate;
		boolean userMissionCheck;
		boolean aislecheck, baycheck, levelcheck, positioncheck, storagebincheck;
		//Clicking Mission button 
		driver.findElement(By.id("2")).click();
		Thread.sleep(2000);
		//clicking the 'Add Mission' button
		driver.findElement(By.id("add_mission_btn")).click();
		Thread.sleep(2000);
		//Entering the 'Mission Name' with time stamp
		Date d = new Date(System.currentTimeMillis());
		userMissionDate = d.toString();
		userMissionNewdate =  userMissionDate.replaceAll("\\s+", "");
	    System.out.println(userMissionNewdate);
		driver.findElement(By.id("add_mission_name")).sendKeys(missionNamePara+userMissionNewdate);
		
		//Selecting the 'Mission Mode'
		WebElement userMissionMode= driver.findElement(By.id("add_select_mission_mode"));
		Select selmode= new Select(userMissionMode);
		selmode.selectByVisibleText(missionModePara);
		Thread.sleep(2000);
		
		//Selecting 'Warehouse'
		WebElement warehouse = driver.findElement(By.id("add_select_warehouse"));
		Select selwarehouse= new Select(warehouse);
		selwarehouse.selectByVisibleText(warehousePara);
		Thread.sleep(2000);
		
		//Entering Position
		driver.findElement(By.id("add_custom_field")).sendKeys(runtimePositionPara);
		Thread.sleep(2000);
		
		//Clicking 'Add Task' button
		driver.findElement(By.id("add_more_tasks_btn")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("add_more_tasks_btn")).click();
		Thread.sleep(2000);
		//Clicking 'Delete Task' button
		driver.findElement(By.id("delete_task2")).click();
		Thread.sleep(2000);
		
		//Entering the 'Inv Document' 
		driver.findElement(By.id("add_inv_document0")).sendKeys(invDocument0Para);
		//Entering Storage Bin
		driver.findElement(By.id("add_storage_bin0")).sendKeys(storageBinPara);
		Thread.sleep(3000);
		
		//To check if the data is added in storage bin field then aisle, bay, level & position fields should be disabled.
		//To check Aisle
		aislecheck = driver.findElement(By.id("add_aisle0")).isEnabled();
		if (aislecheck) {
			System.out.println("Test Failed: Aisle field shouldn't be enabled");
		}
		else {
			System.out.println("Test Passed: Aisle field should be disabled");
		}
		
		//To check Bay
		baycheck = driver.findElement(By.id("add_bay0")).isEnabled();
		if (baycheck) {
			System.out.println("Test Failed: Bay field shouldn't be enabled");
		}
		else {
			System.out.println("Test Passed: Bay field should be disabled");
		}
		
		//To check Level
		levelcheck = driver.findElement(By.id("add_level0")).isEnabled();
		if (levelcheck) {
			System.out.println("Test Failed: Level field shouldn't be enabled");
		}
		else {
			System.out.println("Test Passed: Level field should be disabled");
		}
		
		//To check Position
		positioncheck = driver.findElement(By.id("add_position0")).isEnabled();
		if (positioncheck) {
			System.out.println("Test Failed: Position field shouldn't be enabled");
		}
		else {
			System.out.println("Test Passed: Position field should be disabled");
		}
		
		//Entering the Product Name		
		driver.findElement(By.id("product_name0")).sendKeys(productName0Para);
		//Entering the Serial Number
		driver.findElement(By.id("add_serial_number0")).sendKeys(serialNumber0Para);
		Thread.sleep(3000);
		
		//Entering the 'Inv Document' 
		driver.findElement(By.id("add_inv_document1")).sendKeys(invDocument1Para);
		//Entering Aisle
		driver.findElement(By.id("add_aisle1")).sendKeys(aisle1Para);
		//Entering Bay
		driver.findElement(By.id("add_bay1")).sendKeys(bay1Para);
		//Entering Level
		driver.findElement(By.id("add_level1")).sendKeys(level1Para);
		//Entering Position
		driver.findElement(By.id("add_position1")).sendKeys(position1Para);
		
		//To check if data is entered in aisle, bay, level and position then storage bin field should be disabled
		storagebincheck = driver.findElement(By.id("add_storage_bin1")).isEnabled();
		if (storagebincheck) {
			System.out.println("Test Failed: Storage Bin field shouldn't be enabled");
		}
		else {
			System.out.println("Test Passed: Storage Bin field should be disabled");
		}
		//Entering Product Name			
		driver.findElement(By.id("product_name1")).sendKeys(productName1Para);
		//Entering Serial Name
		driver.findElement(By.id("add_serial_number1")).sendKeys(serialNumber1Para);
		Thread.sleep(3000);
		//Clicking 'Create Mission' button
		driver.findElement(By.id("add_mission_submit_btn")).click();
		Thread.sleep(4000);
		
		//Checking 'User Created Mission' is added successfully or not.
    	userMissionCheck = driver.findElement(By.xpath("//td[contains(text(),'AutoPTCUserMission')]")).isDisplayed();

    	if(userMissionCheck){
    			System.out.println("User Created Mission is added successfully");
    		}
    	else
    		{
    			System.out.println("User Created Mission isn't added successfully");
    		}
    	
    	//Taking the screenshot of Mission screen
    	File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    	FileUtils.copyFile(scrFile, new File("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/screenshots/UserMissionScreen.png"),true);
      	
    	//To verify User Created Mission in database
    	//Storing the collection information in the collection object
    	DBCollection collection = db.getCollection("missions");
		BasicDBObject searchQuery = new BasicDBObject();
		BasicDBObject field1 = new BasicDBObject();
		BasicDBObject field2 = new BasicDBObject();
		
		searchQuery.put("mission_name", missionNamePara+userMissionNewdate);
		field1.put("mission_name","1");	
		field2.put("mission_status","1");	
		
		
		DBCursor cursor1 = collection.find(searchQuery,field1 );
		DBCursor cursor2 = collection.find(searchQuery,field2 );
		//Taking the screenshot of Mission screen
      	
    	//To verify User Created Mission in database
		while (cursor1.hasNext()) {
			System.out.println(cursor1.next());
		}
		while (cursor2.hasNext()) {
			System.out.println(cursor2.next());
			//collection.remove(searchQuery);
		}
		
	}
	
	//Add WMS Request Created Mission
	@DataProvider 
	public Iterator<Object[]> getWmsMISSIONTestData() {
			ArrayList<Object[]> testWmsMISSIONData = TestUtil.getWmsMISSIONDataFromExcel();
			return testWmsMISSIONData.iterator();	
	}
	
	@Test (dataProvider = "getWmsMISSIONTestData", priority= 4)	  
	public void CreateWMSRequestMission(String userIdPara, String pwdPara, String missionNamePara, String missionModePara) throws InterruptedException, IOException {
		boolean wmsRequestCheck, wmsMissionCheck;
		String wmsMissionDate, wmsMissionNewdate;
		//Clicking on logout button
		driver.findElement(By.xpath("//div[@class='user_name']")).click();
		Thread.sleep(2000);
		//Relogin 
		driver.findElement(By.id("email")).sendKeys(userIdPara);
		driver.findElement(By.id("pwd")).sendKeys(pwdPara);
		driver.findElement(By.id("login_btn")).click();
		Thread.sleep(4000);
		//Clicking WMS Request Button
		driver.findElement(By.id("5")).click();
		Thread.sleep(3000);
		//Clicking the refresh button
		driver.findElement(By.id("refresh_btn")).click();
		Thread.sleep(3000);
		wmsRequestCheck= driver.findElement(By.xpath("//td[contains(text(),'WMS-case2PTC')] ")).isDisplayed();	  
		  
		if(wmsRequestCheck) {
			  System.out.println("WMS Request is created Successfully");
			}
		else
		  	{
			  System.out.println("WMS Request isn't created successfully");
			  
		  	}
		//Taking the screenshot of WMS Request screen
		//Check the File Name, WMS Request Status (Received), Warehouse Name, Mission Status (N/A) in the screenshot
    	File scrFile1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    	FileUtils.copyFile(scrFile1, new File("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/screenshots/WmsRequestScreen1.png"),true);

		
		//Clicking create mission button
		driver.findElement(By.id("create_wms_request_btn0")).click();
		Thread.sleep(2000);
		//Creating the time stamp
		Date d = new Date(System.currentTimeMillis());
		wmsMissionDate = d.toString();
		wmsMissionNewdate =  wmsMissionDate.replaceAll("\\s+", "");
		System.out.println(wmsMissionNewdate); 
		//Entering the Mission name
		driver.findElement(By.id("create_wms_mission_name")).sendKeys(missionNamePara+wmsMissionNewdate);
		Thread.sleep(2000);
		//Selecting the Mission mode.
		WebElement wmsMissionMode= driver.findElement(By.id("create_wms_select_mission_mode"));
		Select sl= new Select(wmsMissionMode);
		sl.selectByVisibleText(missionModePara);
		//Clicking the Create Mission button
		driver.findElement(By.id("create_wms_request_submit_btn")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("2")).click();
		Thread.sleep(3000);
		wmsMissionCheck= driver.findElement(By.xpath("//td[contains(text(),'AutoPTCWmsMission')]")).isDisplayed();	  
		  
		if(wmsMissionCheck) {
			  System.out.println("WMS Request Mission is created Successfully");
			}
		else
		  	{
			  System.out.println("WMS Request Mission isn't created successfully");
			  
		  	}
		//Taking the screenshot of Mission screen
    	File scrFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    	FileUtils.copyFile(scrFile2, new File("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/screenshots/WmsMissionScreen.png"),true);
      	
    	
    	//To verify WMS Request Created Mission in database
		DBCollection collection = db.getCollection("mission-views");
		BasicDBObject searchQuery = new BasicDBObject();
		BasicDBObject field1 = new BasicDBObject();
		BasicDBObject field2 = new BasicDBObject();
		
		searchQuery.put("mission_name", missionNamePara+wmsMissionNewdate);
		field1.put("mission_name","1");	
		field2.put("mission_status","1");
		DBCursor cursor1 = collection.find(searchQuery,field1 );
		DBCursor cursor2 = collection.find(searchQuery,field2 );
		
		while (cursor1.hasNext()) {
			System.out.println(cursor1.next());
		}
		while (cursor2.hasNext()) {
			System.out.println(cursor2.next());
			//collection.remove(searchQuery);
		}
		
		//Clicking WMS Request Button
		driver.findElement(By.id("5")).click();
		Thread.sleep(3000);
		//Clicking the refresh button
		driver.findElement(By.id("refresh_btn")).click();
		Thread.sleep(3000);
		
		//Taking the screenshot of WMS Request screen again
		//Check the WMS Request Status (Mission Created), Mission Status (Initialized) in the screenshot
    	File scrFile3 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    	FileUtils.copyFile(scrFile3, new File("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/screenshots/WmsRequestScreen2.png"),true);
		
}
	
	@AfterTest 
	public void TearDown() {
		driver.quit();
		mongoClient.close();	  
	}

}
