package tests;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

import utility.TestUtil;



public class DronewareAdminTest {
	
	WebDriver driver;
	DB db;
	MongoClient mongoClient = null;
	
		  
	@BeforeTest
	//Driver Initialization, Login & Database Connectivity
	@Parameters({"url","email","pwd", "drivertype", "driverpath", "host", "user", "password", "port", "dbname", "dbuser", "dbpwd"})
	public void TestSetup(String url, String email, String pwd, String drivertype, String driverpath, String host, String user, String password, int port, String dbname, String dbuser, String dbpwd) {
		
		//JSch allows to connect to SSH(Secure Socket Shell) server
	    JSch jsch = new JSch();
	    //Session class stores session information for each user for security
	    Session session;		
		Boolean LoginCheck;
				
		System.setProperty(drivertype, driverpath);
		driver = new ChromeDriver();
		driver.get(url);
	    driver.manage().window().maximize();
	    driver.manage().deleteAllCookies();
	    driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.findElement(By.id("email")).sendKeys(email);
		driver.findElement(By.id("pwd")).sendKeys(pwd);
		driver.findElement(By.id("login_btn")).click();
		
		LoginCheck = driver.findElement(By.xpath("//h1[contains(text(),'Organization Information')]")).isDisplayed();

		if(LoginCheck){
			  System.out.println("Login Successful");
		  }
		  else
		  {
			  System.out.println("Login is not successful");
		  }
		
		  
		 //Database Connectivity
		 try {
			 	//Proving the SSH details. GetSession returns the current HttpSession
			    session = jsch.getSession(user, host, port);
			 	session.setPassword(password);
			 	//Disabling the HostKey Checking
			 	java.util.Properties config = new java.util.Properties(); 
			 	config.put("StrictHostKeyChecking", "no");
			 	session.setConfig(config);
			 	session.connect();
			 	//setPortForwardingL() registers the local port forwarding. Here bind address is 'localhost' so listening port will be bound for local use only
			 	int forwardedPort = session.setPortForwardingL(0, "localhost", 27017);

			 	//To solve logger issue
			 	Logger mongoLogger = Logger.getLogger( "org.mongodb.driver" );
			 	mongoLogger.setLevel(Level.SEVERE);
		    
			 	//Creating the Mongo Credential instance by providing database details.
			 	MongoCredential mongoCredential = MongoCredential.createCredential(dbuser, dbname, dbpwd.toCharArray());

			 	//Passing the server address list to the mongo client constructor in order to connect 
			 	mongoClient = new MongoClient(new ServerAddress("localhost", forwardedPort), Arrays.asList(mongoCredential));
		
			 	//Reading the database
			 	db = mongoClient.getDB("droneware");
	 			
		 	} catch (JSchException e) {
		 		
		 		e.printStackTrace();
		 	}
		 	  
   }
	
	//Adding an Organization
	@DataProvider 
	  public Iterator<Object[]> getORGTestData() {
		  //'testORGData' is array list object whcih holds the entire values
		  ArrayList<Object[]> testORGData = TestUtil.getORGDataFromExcel();
		  //Iterator will pick all the values one by one in the sequence and pass it to the test function.
		  return testORGData.iterator();	  
	  }    
    
    @Test (dataProvider = "getORGTestData", priority =1)
    public void AddOrg(String orgLogoPara) throws AWTException, InterruptedException, IOException {
	    String orgdate, orgnewdate;
	    boolean orgAddCheck;
	    driver.findElement(By.id("add_organization_btn")).click();
	    Thread.sleep(2000);
	    Date d = new Date(System.currentTimeMillis());
	    orgdate = d.toString();
        orgnewdate =  orgdate.replaceAll("\\s+","");
        System.out.println(orgnewdate);
	    
        //Entering Organization Name      
	    driver.findElement(By.id("organization_name")).sendKeys("AutoPTC"+orgnewdate);
	    Thread.sleep(2000);
	  
	    //Uploading the logo
	    driver.findElement(By.id("org_logo_file")).click();
	    Thread.sleep(3000);	  
	    StringSelection strSel = new StringSelection(orgLogoPara);
	    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
		
	    //Creating an object for robot class
	    Robot robot = new Robot();
	    //Ctrl+V 
	    robot.keyPress(KeyEvent.VK_CONTROL);
	    robot.keyPress(KeyEvent.VK_V);
	    robot.keyRelease(KeyEvent.VK_CONTROL);
	    robot.keyRelease(KeyEvent.VK_V);			
	    Thread.sleep(3000);
	  
	    robot.keyPress(KeyEvent.VK_ENTER);
	    robot.keyRelease(KeyEvent.VK_ENTER); 
	  
	    //Selecting License Key Start Date
	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    Date date = new Date();	  
	    String startDate = dateFormat.format(date);	  
	    System.out.println("Current date and time is " +startDate); 
	    driver.findElement(By.id("license_key_start_date")).sendKeys(startDate);
	    Thread.sleep(2000);
	  
	    //Selecting License Key End Date
	    Calendar cal = Calendar.getInstance();
	    Date today = cal.getTime();
	    //To get previous year add -1
	    cal.add(Calendar.YEAR, 1); 
	    Date nextYear = cal.getTime();
	    String endDate = dateFormat.format(nextYear);
	  
	    System.out.println("Next Year date and time is " +endDate); 
	  
	    driver.findElement(By.id("license_key_end_date")).sendKeys(endDate);
	    Thread.sleep(3000);
	  
	    //Clicking Save Button
	    driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();
	    Thread.sleep(7000);
	    
		//Checking Organization is added successfully or not	  
	    orgAddCheck = driver.findElement(By.xpath("//td[contains(text(),'AutoPTC')]")).isDisplayed();
		 
	    if(orgAddCheck){
	    	System.out.println("Organization is added successfully");
	    }
	    else
	    {
	    	System.out.println("Organization is not added.");
	    }
	    
	  //Taking the screenshot of Organization screen
	  File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	  FileUtils.copyFile(scrFile, new File("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/screenshots/OrganizationScreen.png"),true);
	  
	  //To verify the organization data in database
	  //Storing the collection information in the collection object
	  DBCollection collection = db.getCollection("organizations");	
	  //BasicDBObject can be used to represent a document in mongodb
	  BasicDBObject searchQuery = new BasicDBObject();
	  //Query
	  searchQuery.put("organization_name", "AutoPTC"+orgnewdate);
	  //Cursor is pointer to the collection of documents. Cursor will be iterated automatically when the result of the query is returned.
	  DBCursor cursor1 = collection.find(searchQuery);
		 
		while (cursor1.hasNext()) {
		    System.out.println(cursor1.next());
		    //collection.remove(searchQuery);
		}
  }
    
  //Add WMS Config  
  @DataProvider 
  public Iterator<Object[]> getWMSTestData() {
	  ArrayList<Object[]> testWMSData = TestUtil.getWMSDataFromExcel();
	  return testWMSData.iterator();	  
  }
  
@Test(dataProvider = "getWMSTestData", priority =2)
public void AddWMSConfig(String wmsTypePara, String wmsIPITPara, String wmsOPITPara, String wmsIPPara) throws InterruptedException, IOException {
	  
	  String wmsdate, wmsnewdate;
	  boolean wmsconfigAddCheck;
	  //Clicking Organization Name
	  driver.findElement(By.xpath("//td[contains(text(),'AutoPTC')]")).click();
	  Thread.sleep(2000);
	  //Clicking WMSConfig button
	  driver.findElement(By.id("add_wms_config_btn")).click();
	  Thread.sleep(2000);
	  Date d = new Date(System.currentTimeMillis());
	  wmsdate = d.toString();
      wmsnewdate =  wmsdate.replaceAll("\\s+", "");
      System.out.println(wmsnewdate);
	  //Entering WMSConfig Name
	  driver.findElement(By.id("wms_name")).sendKeys("AutoPTCWMSConfig"+wmsnewdate);
	  
	  //Selecting WMSConfig Type
	  WebElement wmstype = driver.findElement(By.id("select_wms_type"));
	  Select wms = new Select(wmstype);
	  wms.selectByValue(wmsTypePara);
	  
	  //Selecting WMS I/P Interface Type 
	  WebElement wmsIPIT = driver.findElement(By.id("select_wms_ip_interface_type"));
	  Select wmsip = new Select(wmsIPIT);
	  wmsip.selectByValue(wmsIPITPara);
	  
	  //Selecting WMS O/P Interface Type 
	  WebElement wmsOPIT = driver.findElement(By.id("select_wms_op_interface_type"));
	  Select wmsop = new Select(wmsOPIT);
	  wmsop.selectByValue(wmsOPITPara);
	  
	  //Entering WMS IP
	  driver.findElement(By.id("ip_field")).sendKeys(wmsIPPara);
	  driver.findElement(By.id("add_wms_config_submit_btn")).click();
	  Thread.sleep(5000);
	  
	  
	  //Checking WMSConfig Record is added successfully or not
	  wmsconfigAddCheck = driver.findElement(By.xpath("//td[contains(text(),'AutoPTCWMSConfig')]")).isDisplayed();
		 
	  if(wmsconfigAddCheck){
		  System.out.println("WMS Config record is added successfully");
	  }
	  else
	  {
		  System.out.println("WMS Config record is not added");
	  }
	  
	  //Taking a screenshot of WMSConfig screen	    
	  File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	  FileUtils.copyFile(scrFile, new File("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/screenshots/WMSConfigScreen.png"),true);
	  
	  //To verify the WMS Config in database
	  DBCollection collection = db.getCollection("wms-configs");	  
	  BasicDBObject searchQuery = new BasicDBObject();
	  searchQuery.put("wms_name", "AutoPTCWMSConfig"+wmsnewdate);
	  DBCursor cursor2 = collection.find(searchQuery);
		 
		while (cursor2.hasNext()) {
		    System.out.println(cursor2.next());
		    //collection.remove(searchQuery);
		}
}

//Adding ICS Config
@DataProvider 
public Iterator<Object[]> getICSTestData() {
	  ArrayList<Object[]> testICSData = TestUtil.getICSDataFromExcel();
	  return testICSData.iterator();	  
}

@Test (dataProvider = "getICSTestData", priority =3)
public void AddPTCICSConfig(String icsIPITPara, String icsOPITPara, String icsIPPara) throws InterruptedException, IOException {
	  String icsdate, icsnewdate;
	  boolean icsconfigAddCheck;
	  //Clicking side menu
	  driver.findElement(By.id("sidebarCollapse")).click();
	  Thread.sleep(2000);
	  //Clicking Ics Config	
	  driver.findElement(By.id("ics_config_link")).click();
	  //Clicking add button.
	  driver.findElement(By.id("add_ics_config_btn")).click();
	  //Entering ICS Name	
	  Date d = new Date(System.currentTimeMillis());
	  icsdate = d.toString();
      icsnewdate =  icsdate.replaceAll("\\s+", "");
      System.out.println(icsnewdate);
	  driver.findElement(By.id("add_ics_name")).sendKeys("AutoPTCICSConfig"+icsnewdate);
	  
	  //Selecting ICS I/P Interface Type	
	  WebElement el=  driver.findElement(By.id("select_ics_interface_input_type"));
	  Select select= new Select(el); 
	  select.selectByValue(icsIPITPara);
	  
	  //Selecting ICS O/P Interface Type
	  WebElement el1= driver.findElement(By.xpath("//select[@id='select_ics_interface_output_type']"));
	  Select sl= new Select(el1);
	  sl.selectByValue(icsOPITPara);
	  
	  //Entering ICS IP
	  driver.findElement(By.id("add_ics_ip")).sendKeys(icsIPPara);
	  
	  //Clicking submit button.
	  driver.findElement(By.id("add_ics_config_submit_btn")).click();	  
	  Thread.sleep(7000);
	  
	  //Checking ICS Config Record is added successfully or not
	  icsconfigAddCheck= driver.findElement(By.id("add_ics_config_btn")).isDisplayed();
	  
	  if(icsconfigAddCheck){
				System.out.println("ICS Config Added Successfully");
			 }
	  else
			 {
				 System.out.println("ICS Config Added Unsuccessfully");
			 }
	  
	  //Taking a screenshot of ICSConfig screen	    
	  File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	  FileUtils.copyFile(scrFile, new File("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/screenshots/ICSConfigScreen.png"),true);
	  
	  //To verify the ICS Config in database
	  DBCollection collection = db.getCollection("ics-configs");	  
	  BasicDBObject searchQuery = new BasicDBObject();
	  searchQuery.put("ics_name", "AutoPTCICSConfig"+icsnewdate);
	  DBCursor cursor3 = collection.find(searchQuery);
		 
		while (cursor3.hasNext()) {
		    System.out.println(cursor3.next());
		    //collection.remove(searchQuery);
		}
	}

//Adding SMTP
@DataProvider 
public Iterator<Object[]> getSMTPTestData() {
	  ArrayList<Object[]> testSMTPData = TestUtil.getSMTPDataFromExcel();
	  return testSMTPData.iterator();	  
}

@Test (dataProvider = "getSMTPTestData", priority =4)
public void AddSMTP(String smtpSenderEmailPara, String smtpHostPara, String smtpPortPara, String smtpUserNamePara, String smtpUserPasswordPara) throws InterruptedException, IOException {
	  boolean smtpAddCheck;
	  String smtpdate, smtpnewdate;
	  //Clicking on sideMenu
	  driver.findElement(By.id("sidebarCollapse")).click();	  
	  Thread.sleep(2000);
	  //Clicking on SMTP link
	  driver.findElement(By.id("smtp_link")).click();
	  //Click on Add Button.
	  driver.findElement(By.id("add_smtp_btn")).click();
	  //Entering SMTP Setting Name
	  Date d = new Date(System.currentTimeMillis());
	  smtpdate = d.toString();
      smtpnewdate = smtpdate.replaceAll("\\s+", "");
      System.out.println(smtpdate);
	  driver.findElement(By.id("smtp_setting_name")).sendKeys("AutoPTCSMTP"+smtpnewdate);
	  //Entering SMTP Sender Email
	  driver.findElement(By.id("smtp_sender_email")).sendKeys(smtpSenderEmailPara);
	  //Entering SMTP Host
	  driver.findElement(By.id("smtp_host")).sendKeys(smtpHostPara);
	  //Entering SMTP Port
	  driver.findElement(By.id("smtp_port")).sendKeys(smtpPortPara);
	  //Entering SMTP User Name
	  driver.findElement(By.id("smtp_user")).sendKeys(smtpUserNamePara);
	  //Entering SMTP User Password
	  driver.findElement(By.id("smtp_password")).sendKeys(smtpUserPasswordPara);
	  //Clicking on save button.
	  driver.findElement(By.id("add_smtp_submit_btn")).click();
	  Thread.sleep(7000);	
	  	  
	  smtpAddCheck= driver.findElement(By.id("add_smtp_btn")).isDisplayed();
	  if(smtpAddCheck){
				System.out.println("SMTP is added successfully");
			 }
	  else
			 {
				 System.out.println("SMTP is not added successfully");
			 }
	  
	  //Taking a screenshot of SMTP screen	    
	  File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	  FileUtils.copyFile(scrFile, new File("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/screenshots/SMTPScreen.png"),true);
	  Thread.sleep(3000);	
	  
	  //To verify the SMTP in database
	  DBCollection collection = db.getCollection("smtp_settings");	  
	  BasicDBObject searchQuery = new BasicDBObject();
	  searchQuery.put("smtp_setting_name", "AutoPTCSMTP"+smtpnewdate);
	  DBCursor cursor4 = collection.find(searchQuery);
		 
	  while (cursor4.hasNext()) {
		    System.out.println(cursor4.next());
		    //collection.remove(searchQuery);
	  }
	  
	 }

//Adding User
@DataProvider 
public Iterator<Object[]> getUSERTestData() {
	   ArrayList<Object[]> testUSERData = TestUtil.getUSERDataFromExcel();
	   return testUSERData.iterator();
}


@Test (dataProvider = "getUSERTestData", priority =5, alwaysRun = true)
public void AddUser(String firstNamePara, String lastNamePara) throws InterruptedException, IOException {
	  
	  String userdate, usernewdate;	  
	  boolean userAddCheck;
	  driver.findElement(By.id("sidebarCollapse")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.id("users_link")).click();
	  //Clicking Add User
	  driver.findElement(By.id("add_user_btn")).click();
	  //Entering First Name
	  driver.findElement(By.id("add_first_name")).sendKeys(firstNamePara);
	  //Entering Last Name 
	  driver.findElement(By.id("add_last_name")).sendKeys(lastNamePara);
	  //Entering Email id
	  Date d = new Date(System.currentTimeMillis());
	  userdate = d.toString();
      usernewdate =  userdate.replaceAll("\\s+", "");
      System.out.println(usernewdate);	 
	  driver.findElement(By.id("add_email")).sendKeys("pankaj.kumar@konverge.ai"+usernewdate);
	  //Clicking submit button
	  driver.findElement(By.id("add_user_submit_btn")).click();
	  
	  //driver.get("https://ddev.apps.emergys.com/user/view-user");
	  driver.navigate().refresh();
	  driver.findElement(By.id("organization_name_link0")).click();
	  Thread.sleep(7000);
	  
	  driver.findElement(By.id("sidebarCollapse")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.id("users_link")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.id("close_menu_sidebar_btn")).click();
	  Thread.sleep(2000);
	 
	   userAddCheck = driver.findElement(By.xpath("//td[contains(text(),'AutoPankaj')]")).isDisplayed();
	   if(userAddCheck){
			System.out.println("User added successfully");
			 }
			 else
			 {
			 System.out.println("User is not added successfully");
			 } 
	  
	  //Taking a screenshot of Users screen	    
	  File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	  FileUtils.copyFile(scrFile, new File("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/screenshots/UserScreen.png"),true);
	  
	  //To verify the Users in database
	  DBCollection collection = db.getCollection("users");	  
	  BasicDBObject searchQuery = new BasicDBObject();
	  searchQuery.put("email", "pankaj.kumar@konverge.ai"+usernewdate);
	  DBCursor cursor5 = collection.find(searchQuery);
		 
	  while (cursor5.hasNext()) {
		    System.out.println(cursor5.next());
		    //collection.remove(searchQuery);
	  }
	}
  

//Adding Warehouse
@DataProvider 
public Iterator<Object[]> getWAREHOUSETestData() {
	   ArrayList<Object[]> testWAREHOUSEData = TestUtil.getWAREHOUSEDataFromExcel();	
	   return testWAREHOUSEData.iterator();
}

@Test (dataProvider = "getWAREHOUSETestData", priority =6, alwaysRun = true)
public void AddWarehouse(String warehouseNumberPara, String aisleNamePara, String bayPara, String bayValuePara, String levelPara, String levelValuePara, String positionPara, String positionValuePara, String labelPara, String aisleDropdownPara, String addBayPara, String addLevelPara) throws InterruptedException, IOException {
	  String warehousedate, warehousenewdate;
	  boolean warehouseAddCheck;
	  //Adding Warehouse
	  Thread.sleep(2000);
	  //Menu
	  driver.findElement(By.id("sidebarCollapse")).click();	  
	  Thread.sleep(2000);
	  //Warehouse Link
	  driver.findElement((By.id("warehouse_link"))).click();
	  //Add Warehouse button
	  
	  driver.findElement(By.id("add_warehouse_btn")).click();
	  Date d = new Date(System.currentTimeMillis());
	  warehousedate = d.toString();
	  warehousenewdate =  warehousedate.replaceAll("\\s+", "");
      System.out.println(warehousenewdate);
	  //Entering warehouse name
	  driver.findElement(By.id("add_warehouse_name")).sendKeys("AutoPTCWarehouse"+warehousenewdate);
	  //Entering warehouse number
	  driver.findElement(By.id("add_warehouse_number")).sendKeys(warehouseNumberPara);
	  
	  //Selecting WMS Name
	  WebElement wmsname = driver.findElement(By.id("add_select_wms_name"));
	  Select wmsn = new Select(wmsname);
	  wmsn.selectByIndex(1);
	  Thread.sleep(2000);
	  
	  //Selecting ICS Name
	  WebElement icsname = driver.findElement(By.id("add_select_ics_name"));
	  Select icsn = new Select(icsname);
	  icsn.selectByIndex(1);
	  Thread.sleep(2000);	  
	  
	  //Aisle Name 
	  driver.findElement(By.id("add_aisle_name")).sendKeys(aisleNamePara);
	  
	  //Entering Bay
	  driver.findElement(By.xpath("/html[1]/body[1]/app-root[1]/app-full-layout[1]/div[1]/div[2]/div[2]/div[1]/div[1]/app-add-warehouse[1]/div[1]/div[1]/form[1]/div[7]/div[2]/div[1]/div[1]/label[2]/input[1]")).sendKeys(bayPara);
	  driver.findElement(By.xpath("/html[1]/body[1]/app-root[1]/app-full-layout[1]/div[1]/div[2]/div[2]/div[1]/div[1]/app-add-warehouse[1]/div[1]/div[1]/form[1]/div[7]/div[2]/div[1]/div[1]/label[4]/input[1]")).sendKeys(bayValuePara);
	  
	  //Entering Level
	  driver.findElement(By.xpath("/html[1]/body[1]/app-root[1]/app-full-layout[1]/div[1]/div[2]/div[2]/div[1]/div[1]/app-add-warehouse[1]/div[1]/div[1]/form[1]/div[7]/div[2]/div[2]/div[1]/label[2]/input[1]")).sendKeys(levelPara);
	  driver.findElement(By.xpath("/html[1]/body[1]/app-root[1]/app-full-layout[1]/div[1]/div[2]/div[2]/div[1]/div[1]/app-add-warehouse[1]/div[1]/div[1]/form[1]/div[7]/div[2]/div[2]/div[1]/label[4]/input[1]")).sendKeys(levelValuePara);
	  
	  WebDriverWait wait5 = new WebDriverWait(driver, 20);
	  wait5.until(ExpectedConditions.elementToBeClickable(By.id("add_custom_field_btn"))).click();
	  
	  //Adding data in custom field
	  driver.findElement(By.id("add_field_value_length")).sendKeys(positionValuePara);
	  driver.findElement(By.id("add_field_name")).sendKeys(positionPara);
	  
	  //Selecting checkbox
	  driver.findElement(By.id("check_box")).click();
	  
	  //Next button
	  driver.findElement(By.id("warehouse_next_btn")).click();
	  Thread.sleep(4000);
	  
	  /*WebElement elm = driver.findElement(By.id(""));
	  JavascriptExecutor jse = (JavascriptExecutor)driver; 
	  jse.executeScript("window.scrollBy(1000,0)");
	  Thread.sleep(2000);	*/
	  
	  //Label
	  driver.findElement(By.id("aisleLabel")).sendKeys(labelPara);
	  driver.findElement(By.id("add_btn")).click();
	  Thread.sleep(3000);
	  
	  //Selecting Aisle
	  WebElement aisleDr = driver.findElement(By.id("aisleDropdown"));
	  Select air = new Select(aisleDr);
	  air.selectByVisibleText(aisleDropdownPara);
	  Thread.sleep(4000);	
	  
	  //Bay
	  driver.findElement(By.id("PTCBay")).sendKeys(addBayPara);
	  Thread.sleep(2000);
	  //Level
	  driver.findElement(By.id("PTCLevel")).sendKeys(addLevelPara);
	  Thread.sleep(2000);
	  
	  //Clicking Add button
	  driver.findElement(By.id("AddRectangle")).click();
	  Thread.sleep(4000);	
	  
	  //Clicking Edit Canvas button
	  driver.findElement(By.xpath("//span[@class='slider round']")).click();
	  Thread.sleep(2000);
	  
	  //Clicking Save button
	  driver.findElement(By.id("save_warehouse_btn")).click();
	  Thread.sleep(5000);
	  
	  //Checking warehouse is added successfully or not.
	  warehouseAddCheck = driver.findElement(By.xpath("//td[contains(text(),'AutoPTCWarehouse')]")).isDisplayed();

	  if(warehouseAddCheck){
			  System.out.println("Warehouse is added successfully");
		  }
	  else
		  {
			  System.out.println("Warehouse isn't added successfully");
		  }
		
		  //Taking a screenshot of Warehouse screen	    
		  File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		  FileUtils.copyFile(scrFile, new File("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/screenshots/WarehouseScreen.png"),true);
		  
		  //To verify the Warehouse data in database
		  DBCollection collection = db.getCollection("warehouses");	  
		  BasicDBObject searchQuery = new BasicDBObject();
		  searchQuery.put("warehouse_name", "AutoPTCWarehouse"+warehousenewdate);
		  DBCursor cursor6 = collection.find(searchQuery);
			 
		  while (cursor6.hasNext()) {
			    System.out.println(cursor6.next());
			    //collection.remove(searchQuery);
		  }
	  }
@AfterTest 
public void TearDown() {
	  driver.close();
	  mongoClient.close();	  
	}

  
}
