package utility;

import java.util.ArrayList;
import java.util.Arrays;

public class TestUtil {
	
	static Xls_Reader reader;
	
	//Function to read Organization Data
	public static ArrayList<Object[]> getORGDataFromExcel(){
		//Creating the ArrayList object 'myORGData'which can contain the object array
		ArrayList<Object[]> myORGData = new ArrayList<Object[]>();
		
		try {
			//Creating the object of Xls_Reader class. In this class std methods are provided by excel automation in java to read the data from excelsheet.
			reader = new Xls_Reader("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/testdata/Droneware.xlsx");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		//Reading the data from excelsheet and storing it in object array.
		for (int rowNum=2; rowNum<=reader.getRowCount("AddOrg"); rowNum++) {
			//Reading the row wise data from excelsheet and storing it in object array.
			String orgLogoPara = reader.getCellData("AddOrg", "orglogofilepath", rowNum);
			Object ob[] = {orgLogoPara};
			//Storing the object array in the ArrayList
			myORGData.add(ob);
		}
		
		return myORGData;
				
	}
	
	
	//Function to read WMS Config Data
	public static ArrayList<Object[]> getWMSDataFromExcel(){
		ArrayList<Object[]> myWMSData = new ArrayList<Object[]>();
		
		try {
			
			reader = new Xls_Reader("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/testdata/Droneware.xlsx");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		for (int rowNum=2; rowNum<=reader.getRowCount("AddWMSConfig"); rowNum++) {
			
			String wmsTypePara = reader.getCellData("AddWMSConfig", "wmstype", rowNum);
			String wmsIPITPara = reader.getCellData("AddWMSConfig", "wmsinputinterfacetype ", rowNum);
			String wmsOPITPara = reader.getCellData("AddWMSConfig", "wmsoutputinterfacetype ", rowNum);
			String wmsIPPara = reader.getCellData("AddWMSConfig", "wmsip", rowNum);		
			
			Object ob[] = {wmsTypePara,wmsIPITPara,wmsOPITPara,wmsIPPara};
			myWMSData.add(ob);
		}		
		return myWMSData;
				
	}
	
	//Function to read ICS Config Data
	public static ArrayList<Object[]> getICSDataFromExcel(){
		ArrayList<Object[]> myICSData = new ArrayList<Object[]>();
		
		try {
			
			reader = new Xls_Reader("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/testdata/Droneware.xlsx");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		for (int rowNum=2; rowNum<=reader.getRowCount("AddICSConfig"); rowNum++) {
			
			String icsIPITPara = reader.getCellData("AddICSConfig", "icsinputinterfacetype ", rowNum);
			String icsOPITPara = reader.getCellData("AddICSConfig", "icsoutputinterfacetype ", rowNum);
			String icsIPPara = reader.getCellData("AddICSConfig", "icsip", rowNum);			
			Object ob[] = {icsIPITPara,icsOPITPara,icsIPPara};
			myICSData.add(ob);
		}
		System.out.println("My Data1:"+Arrays.toString(myICSData.get(0)));
		System.out.println("My Data2:"+Arrays.deepToString(myICSData.toArray()));
		return myICSData;
				
	}
	
	//Function to read SMTP Data	
	public static ArrayList<Object[]> getSMTPDataFromExcel(){
		ArrayList<Object[]> mySMTPData = new ArrayList<Object[]>();
		
		try {
			
			   reader = new Xls_Reader("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/testdata/Droneware.xlsx");
		    }
		catch(Exception e){
			e.printStackTrace();
		}
		
		for (int rowNum=2; rowNum<=reader.getRowCount("AddSMTP"); rowNum++) {
			
			String smtpSenderEmailPara = reader.getCellData("AddSMTP", "smtpsenderemail", rowNum);
			String smtpHostPara = reader.getCellData("AddSMTP", "smtphost", rowNum);
			String smtpPortPara = reader.getCellData("AddSMTP", "smtpport", rowNum);
			String smtpUserNamePara = reader.getCellData("AddSMTP", "smtpusername", rowNum);
			String smtpUserPasswordPara = reader.getCellData("AddSMTP", "smtpuserpassword", rowNum);
			
			Object ob[] = {smtpSenderEmailPara, smtpHostPara, smtpPortPara, smtpUserNamePara, smtpUserPasswordPara};
			mySMTPData.add(ob);
		}		
		return mySMTPData;
	}
	
	
		//Function to read (Admin Side) User Data	
		public static ArrayList<Object[]> getUSERDataFromExcel(){
			ArrayList<Object[]> myUSERData = new ArrayList<Object[]>();
			
			try {
				
				reader = new Xls_Reader("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/testdata/Droneware.xlsx");
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
			for (int rowNum=2; rowNum<=reader.getRowCount("AddUser"); rowNum++) {
				
				String firstNamePara = reader.getCellData("AddUser", "firstname", rowNum);
				String lastNamePara = reader.getCellData("AddUser", "lastname", rowNum);
				Object ob[] = {firstNamePara, lastNamePara};
				myUSERData.add(ob);
			}		

			return myUSERData;
		}
		
		//Function to read Warehouse Data	
		public static ArrayList<Object[]> getWAREHOUSEDataFromExcel(){
			ArrayList<Object[]> myWAREHOUSEData = new ArrayList<Object[]>();
			
			try {
				
					reader = new Xls_Reader("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/testdata/Droneware.xlsx");
			    }
			catch(Exception e){
					e.printStackTrace();
			   }
			
			for (int rowNum=2; rowNum<=reader.getRowCount("AddWarehouse"); rowNum++) {
				
				String warehouseNumberPara = reader.getCellData("AddWarehouse", "warehousenumber", rowNum);
				String aisleNamePara = reader.getCellData("AddWarehouse", "aislename", rowNum);
				String bayPara = reader.getCellData("AddWarehouse", "bay", rowNum);
				String bayValuePara = reader.getCellData("AddWarehouse", "bayvalue", rowNum);
				String levelPara = reader.getCellData("AddWarehouse", "level", rowNum);
				String levelValuePara = reader.getCellData("AddWarehouse", "levelvalue", rowNum);
				String positionPara = reader.getCellData("AddWarehouse", "position", rowNum);
				String positionValuePara = reader.getCellData("AddWarehouse", "positionvalue", rowNum);
				String labelPara = reader.getCellData("AddWarehouse", "label", rowNum);
				String aisleDropdownPara = reader.getCellData("AddWarehouse", "aisledropdown", rowNum);
				String addBayPara = reader.getCellData("AddWarehouse", "addbay", rowNum);
				String addLevelPara = reader.getCellData("AddWarehouse", "addlevel", rowNum);
				
				Object ob[] = {warehouseNumberPara, aisleNamePara, bayPara, bayValuePara, levelPara, levelValuePara, positionPara, positionValuePara, labelPara, aisleDropdownPara, addBayPara, addLevelPara};
				myWAREHOUSEData.add(ob);
			}		
			return myWAREHOUSEData;
		}
		
		//Function to read Drone Data	
		 public static ArrayList<Object[]> getDRONEDataFromExcel(){
			ArrayList<Object[]> myDRONEData = new ArrayList<Object[]>();
					
			try {
					reader = new Xls_Reader("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/testdata/Droneware.xlsx");
				}
			catch(Exception e){
					e.printStackTrace();
				}
					
			for (int rowNum=2; rowNum<=reader.getRowCount("AddDrone"); rowNum++) {
						
					String droneLogoPara = reader.getCellData("AddDrone", "dronelogofilepath", rowNum);
					String connectionTypePara = reader.getCellData("AddDrone", "droneconnectiontype", rowNum);
					String droneIpPara = reader.getCellData("AddDrone", "droneip", rowNum);
					String batteryChargePara = reader.getCellData("AddDrone", "batterycharge", rowNum);						
					String warehousePara = reader.getCellData("AddDrone", "warehouse", rowNum);
					String modalNumberPara = reader.getCellData("AddDrone", "modalnumber", rowNum);
					String serialNumberPara = reader.getCellData("AddDrone", "serialnumber", rowNum);
					String macAddress = reader.getCellData("AddDrone", "macaddress", rowNum);
					String otherDetailsPara = reader.getCellData("AddDrone", "otherdetails", rowNum);
												
					Object ob[] = {droneLogoPara, connectionTypePara, droneIpPara, batteryChargePara, warehousePara, modalNumberPara, serialNumberPara, macAddress, otherDetailsPara};
					myDRONEData.add(ob);
				}		
				return myDRONEData;
			}
		 
		 
		//Function to read (User Side) User Data	
		public static ArrayList<Object[]> getDroneUSERDataFromExcel(){
			ArrayList<Object[]> myDroneUSERData = new ArrayList<Object[]>();
				
			try {
					reader = new Xls_Reader("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/testdata/Droneware.xlsx");
				}
			catch(Exception e){
					e.printStackTrace();
				}
				
			for (int rowNum=2; rowNum<=reader.getRowCount("AddDroneUser"); rowNum++) {
					
					String dfirstNamePara = reader.getCellData("AddDroneUser", "firstname", rowNum);
					String dlastNamePara = reader.getCellData("AddDroneUser", "lastname", rowNum);
					String demailPara = reader.getCellData("AddDroneUser", "email", rowNum);
					String duserTypePara = reader.getCellData("AddDroneUser", "usertype", rowNum);
					
					Object ob[] = {dfirstNamePara, dlastNamePara, demailPara, duserTypePara};
					myDroneUSERData.add(ob);
			}		

				return myDroneUSERData;
		}
		
		//Function to read User Created Mission Data
		public static ArrayList<Object[]> getUserMISSIONDataFromExcel(){
			ArrayList<Object[]> myUserMISSIONData = new ArrayList<Object[]>();
						
			try {
					reader = new Xls_Reader("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/testdata/Droneware.xlsx");
				}
			catch(Exception e){
					e.printStackTrace();
				}
						
			for (int rowNum=2; rowNum<=reader.getRowCount("AddUserMission"); rowNum++) {
							
					String missionNamePara = reader.getCellData("AddUserMission", "missionname", rowNum);
					String missionModePara = reader.getCellData("AddUserMission", "missionmode", rowNum);
					String warehousePara = reader.getCellData("AddUserMission", "warehouse", rowNum);
					String runtimePositionPara = reader.getCellData("AddUserMission", "runtimeposition", rowNum);
					
					String invDocument0Para = reader.getCellData("AddUserMission", "invdocument0", rowNum);		
					String storageBinPara = reader.getCellData("AddUserMission", "storagebin", rowNum);
					
					String productName0Para = reader.getCellData("AddUserMission", "productname0", rowNum);
					String serialNumber0Para = reader.getCellData("AddUserMission", "serialnumber0", rowNum);

					
					String invDocument1Para = reader.getCellData("AddUserMission", "invdocument1", rowNum);		

					
					String aisle1Para = reader.getCellData("AddUserMission", "aisle1", rowNum);
					String bay1Para = reader.getCellData("AddUserMission", "bay1", rowNum);
					
					String level1Para = reader.getCellData("AddUserMission", "level1", rowNum);
					String position1Para = reader.getCellData("AddUserMission", "position1", rowNum);
					String productName1Para = reader.getCellData("AddUserMission", "productname1", rowNum);
					String serialNumber1Para = reader.getCellData("AddUserMission", "serialnumber1", rowNum);
					
					Object ob[] = {missionNamePara, missionModePara, warehousePara, runtimePositionPara, invDocument0Para, storageBinPara, productName0Para, serialNumber0Para, invDocument1Para, aisle1Para, bay1Para, level1Para, position1Para, productName1Para, serialNumber1Para};
					myUserMISSIONData.add(ob);
				}		

					return myUserMISSIONData;
			}
		
		//Function to read WMS Request Created Mission Data
		public static ArrayList<Object[]> getWmsMISSIONDataFromExcel(){
			ArrayList<Object[]> myWmsMISSIONData = new ArrayList<Object[]>();
								
			try {
					reader = new Xls_Reader("/home/konverge/eclipse-workspace/maven-droneware/src/test/java/testdata/Droneware.xlsx");
				}
			catch(Exception e){
						e.printStackTrace();
				}
								
			for (int rowNum=2; rowNum<=reader.getRowCount("AddWmsMission"); rowNum++) {
					
					String userIdPara = reader.getCellData("AddWmsMission", "userid", rowNum);
					String pwdPara = reader.getCellData("AddWmsMission", "pwd", rowNum);
					String missionNamePara = reader.getCellData("AddWmsMission", "missionname", rowNum);
					String missionModePara = reader.getCellData("AddWmsMission", "missionmode", rowNum);
							
					Object ob[] = {userIdPara, pwdPara, missionNamePara, missionModePara};
					myWmsMISSIONData.add(ob);
				}		

					return myWmsMISSIONData;
			}	

}
